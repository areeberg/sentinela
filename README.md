# Sentinel

  - This repository provides supplementary material for the paper cited below

## Concrete Dam Inspection with UAV Imagery and DCNN-based Object Detection
### Authors:
- Alexandre R de Mello, Flávio G O Barbosa, Murilo L Fonseca from  SENAI Innovation Institute of Embedded Systems of Santa Catarina
- Camila D Smirdele from Dona Francisca Energética S.A.

### Overview
The Sentinel dataset is a labeled set of images related to  a  roller-compacted  concrete  water  dam  with  a  Creager profile  spillway to identify damage or unwanted objects.  We  performed  two  UAV  missions  with  an automatic route (on different days with diverse luminosity) to collect images to create the dataset, in which the first mission was performed on a sunny morning and the latter on a cloudy afternoon. Each mission generates 1087 images, and there is a 50% overlap between images that may vary according to the UAV sensors and GPS positioning.

### Description
- The original image size is 4000 x 3000.
- For the training and validation sets we split each image into 4 sub-images of size 2000 x 1125.
- The training and validation split ratio is 70% and 30% respectively.
- We did not use images on the training or validation set that were not annotated 
- The training set has 209 images (2000 x 1125) with 357 annotations of 23 objects and 334 damages.
- The validation set has 95 images (2000 x 1125) with 172 annotations of 12 objects and 160 damages.
- The test set has 316 images (4000 x 3000) with 316 annotations of 84 objects, 43 damages and 189 images without object or damage.

### Sample images

Sample 1
![](samples/2DJI_0725_01_01.jpg "Example 1")

Sample 2
![](samples/2DJI_0730_01_02.jpg "Example 2")

Sample 3
![](samples/DJI_0637_02_01.jpg "Example 3")



### Further information

We include the Faster RCNN with (resnetv2 as backbone) that we use to train our model and the necessary configuration files to train a new model.

### Cite

Please refer to this dataset as:
Concrete Dam Inspection with UAV Imagery and DCNN-based Object Detection
AR De Mello, FGO Barbosa, ML Fonseca, CD Smiderle
2021 IEEE International Conference on Imaging Systems and Techniques (IST), 1-6
https://ieeexplore.ieee.org/abstract/document/9651348/

### License

MIT